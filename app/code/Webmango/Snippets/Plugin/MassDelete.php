<?php

namespace Webmango\Snippets\Plugin;

use Magento\Cms\Model\ResourceModel\Block\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;
use Webmango\Snippets\Model\ImageUploader;
use \Magento\Cms\Model\BlockRepository;

Class MassDelete
{
    protected $imageUploader;

    protected $blockRepository;

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    public function __construct(
        BlockRepository $blockRepository,
        ImageUploader $imageUploader,
        Filter $filter,
        CollectionFactory $collectionFactory
    )
    {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->blockRepository = $blockRepository;
        $this->imageUploader = $imageUploader;
    }

    public function beforeExecute(\Magento\Cms\Controller\Adminhtml\Block\MassDelete $subject)
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        foreach ($collection as $block) {
            try {
                $blockImageName = $block->getImage();
                $this->imageUploader->deleteFile($blockImageName);
            } catch (\Exception $e) {
            }
        }
    }
}
