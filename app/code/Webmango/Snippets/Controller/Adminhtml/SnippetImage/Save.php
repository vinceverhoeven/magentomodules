<?php
/**
* 11-03-20
* Author Vince Verhoeven
*/

namespace Webmango\Snippets\Controller\Adminhtml\SnippetImage;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Backend\App\Action\Context;
use Magento\Cms\Api\BlockRepositoryInterface;
use Magento\Cms\Model\Block;
use Magento\Cms\Model\BlockFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Registry;
use Webmango\Snippets\Model\ImageUploader;
use Webmango\Snippets\Helper\Data as SnippetHelper;

/**
 * Save CMS block action.
 */
class Save extends \Magento\Cms\Controller\Adminhtml\Block implements HttpPostActionInterface
{
    protected $_snippetHelper;
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var BlockFactory
     */
    private $blockFactory;

    protected $imageUploader;

    /**
     * @var BlockRepositoryInterface
     */
    private $blockRepository;

    public function __construct(
        SnippetHelper $snippetHelper,
        Context $context,
        Registry $coreRegistry,
        DataPersistorInterface $dataPersistor,
        BlockFactory $blockFactory = null,
        BlockRepositoryInterface $blockRepository = null,
        ImageUploader $imageUploader
    )
    {
        $this->_snippetHelper = $snippetHelper;
        $this->imageUploader = $imageUploader;
        $this->dataPersistor = $dataPersistor;
        $this->blockFactory = $blockFactory
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(BlockFactory::class);
        $this->blockRepository = $blockRepository
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(BlockRepositoryInterface::class);
        parent::__construct($context, $coreRegistry);
    }

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        $data['not_from_gallery'] = false;
        $data['tmpurl'] = "/";
        $uploadedNewImage = false;

        if ($data) {
            if (isset($data['is_active']) && $data['is_active'] === 'true') {
                $data['is_active'] = Block::STATUS_ENABLED;
            }
            if (empty($data['block_id'])) {
                $data['block_id'] = null;
            }

            if (isset($data['image']) && !empty($data['image'][0]["name"])) {
                $dataImageCopy = $data['image'];
                // if uploaded with upload button and not through gallery
                if (isset($data['image'][0]['not_from_gallery'])) {
                    $data['not_from_gallery'] = true;
                    $data['tmpurl'] = $this->imageUploader->getBaseTmpPath();
                }

                // here we override image value because needs to only save image name
                $data['image'] = $dataImageCopy[0]["name"];
                if ($this->_snippetHelper->getNewImageSession()) {
                    $uploadedNewImage = true;
                    $data['image'] = $this->imageUploader->getNewFileName($dataImageCopy[0]["name"]);
                }
            }
            $this->_snippetHelper->unsetNewImageSession();

            /** @var \Magento\Cms\Model\Block $model */
            $model = $this->blockFactory->create();
            $id = $this->getRequest()->getParam('block_id');
            if ($id) {
                try {
                    $model = $this->blockRepository->getById($id);

                    if ($uploadedNewImage && $model) {
                        $blockImageName = $model->getImage();
                        $this->imageUploader->deleteFile($blockImageName);
                    }

                    // if cleared image
                    if (!isset($data['image']) && !empty($model->getImage())) {
                        $blockImageName = $model->getImage();
                        $this->imageUploader->deleteFile($blockImageName);
                    }

                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage(__('This block no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }

            $model->setData($data);

            try {
                $this->blockRepository->save($model);
                if ($uploadedNewImage) {
                    // means used upload button
                    if ($data['not_from_gallery']) {
                        $this->imageUploader->moveFileFromTmp($data['tmpurl'], $dataImageCopy[0]["name"], $data['image'], true);
                    } else {
                        $this->imageUploader->moveFileFromTmp("", $dataImageCopy[0]["name"], $data['image'], false);
                    }
                }
                $this->messageManager->addSuccessMessage(__('You saved the block.'));
                $this->dataPersistor->clear('cms_block');
                return $this->processBlockReturn($model, $data, $resultRedirect);
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the block.'));
            }

            $this->dataPersistor->set('cms_block', $data);
            return $resultRedirect->setPath('*/*/edit', ['block_id' => $id]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Process and set the block return
     *
     * @param \Magento\Cms\Model\Block $model
     * @param array $data
     * @param \Magento\Framework\Controller\ResultInterface $resultRedirect
     * @return \Magento\Framework\Controller\ResultInterface
     */
    private function processBlockReturn($model, $data, $resultRedirect)
    {
        $redirect = $data['back'] ?? 'close';
        if ($redirect === 'continue') {
            $resultRedirect->setPath('*/*/edit', ['block_id' => $model->getId()]);
        } else if ($redirect === 'close') {
            $resultRedirect->setPath('*/*/');
        } else if ($redirect === 'duplicate') {
            $duplicateModel = $this->blockFactory->create(['data' => $data]);
            $duplicateModel->setId(null);
            $duplicateModel->setIdentifier($data['identifier'] . '-' . uniqid());
            $duplicateModel->setIsActive(Block::STATUS_DISABLED);
            $duplicateModel->setImage('');
            $this->blockRepository->save($duplicateModel);
            $id = $duplicateModel->getId();
            $this->messageManager->addSuccessMessage(__('You duplicated the block.'));
            $this->dataPersistor->set('cms_block', $data);
            $resultRedirect->setPath('*/*/edit', ['block_id' => $id]);
        }
        return $resultRedirect;
    }

}
