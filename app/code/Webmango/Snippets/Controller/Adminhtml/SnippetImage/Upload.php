<?php

namespace Webmango\Snippets\Controller\Adminhtml\SnippetImage;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Webmango\Snippets\Helper\Data as SnippetHelper;

/**
 * Save CMS block action.
 */
class Upload extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    protected $_snippetHelper;

    protected $imageUploader;

    public function __construct(
        SnippetHelper $snippetHelper,
        \Magento\Backend\App\Action\Context $context,
        \Webmango\Snippets\Model\ImageUploader $imageUploader
    ) {
        parent::__construct($context);
        $this->imageUploader = $imageUploader;
        $this->_snippetHelper = $snippetHelper;
    }

    /**
     * Upload file controller action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $imageId = $this->_request->getParam('param_name', 'webmango_snippet_image');
        try {
            $result = $this->imageUploader->saveFileToTmpDir($imageId);
            $this->_snippetHelper->setNewImageSession(true);
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }
        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
}
